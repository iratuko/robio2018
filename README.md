# ROBIO2018

Robio2018

Deadline : July 15th, 2018

Location : Malaysia

Day : Dec.12-15,2018

URL"https://iratuko@bitbucket.org/iratuko/robio2018.git"


論文目次（仮）

1. はじめに

2. 関連研究

3. 能動的振動入力による手先位置推定

    1. ABCSS
	
    2. 手先位置推定装置
	
    3. 推定用入力振動
	
    4. 機械学習を用いた手先位置推定
	
    5. アプリケーション
	
4. 精度評価実験

    1. 実験環境
	
    2. 各スペクトルごとの比較実験
	
    3. 機械学習手法ごとorカーネルごと比較
	
    4. 精度とトレーニングデータ数の関係
	
    5. 手先位置推定の精度評価
	
5. まとめ